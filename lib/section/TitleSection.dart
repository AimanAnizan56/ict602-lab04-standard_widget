import 'package:flutter/material.dart';

Widget titleSection = Container(
  padding: const EdgeInsets.all(32),
  child: Row(
    children: [
      Expanded(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.only(bottom: 8),
              child: const Text(
                'Kenyir Lake',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16
                )
              ),
            ),
            Text(
              'Hulu Terengganu, Terengganu',
              style: TextStyle(
                fontWeight: FontWeight.w600,
                color: Colors.grey[500],
              ),
            )
          ],
        )
      ),
      Icon(
        Icons.star,
        color: Colors.orange[500],
      ),
      const Text('53'),
    ],
  ),
);