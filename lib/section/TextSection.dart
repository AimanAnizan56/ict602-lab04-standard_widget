import 'package:flutter/material.dart';

Widget textSection = const Padding(
  padding: EdgeInsets.all(32),
  child: Text(
    'Tasik Kenyir or Kenyir Lake is an artificial lake located in '
    'Hulu Terengganu, Terengganu. Malaysia created in 1985 by the '
    'Kenyir Dam on the Kenyir River. The lake provides water to the '
    'Sultan Mahmud Power Station. It is the largest man-made lake in '
    'mainland South East Asia with an area of 260,000 hectares',
    softWrap: true,
    style: TextStyle(
      height: 1.7
    ),
  ),
);